Steps to make it work

* Download all the code
* Build the solution
* Make sure folder "Files" exists in BondoraTest project(the mvc project)
* Make sure there are 2 startup projects: BondoraTest and InvoiceService
* put/create "Products.txt" file in C:/Users folder and fill it with data in following format: <id>;<name>;<type> (example: 3;Komatsu crane;Heavy)
* run the solution (F5)