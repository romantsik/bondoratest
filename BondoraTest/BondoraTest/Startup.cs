﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BondoraTest.Startup))]
namespace BondoraTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
