﻿using System.Web.Mvc;

namespace BondoraTest.Controllers
{
    public class HomeController : Controller
    {

        public HomeController()
        {
            
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home";
            return View();
        }

    }
}