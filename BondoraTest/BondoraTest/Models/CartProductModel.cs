﻿using Microsoft.AspNet.Identity.Owin;

namespace BondoraTest.Models
{
    public class CartProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; } 
    }
}