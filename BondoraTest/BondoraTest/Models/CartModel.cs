﻿using System.Collections.Generic;

namespace BondoraTest.Models
{
    public class CartModel
    {
        public List<CartProductModel> CartProductModels { get; set; }
    }
}